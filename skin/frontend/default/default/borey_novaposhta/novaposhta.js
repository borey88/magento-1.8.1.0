NovaPoshta = Class.create({}, {
    form:               null,
    shippingFormId:    'co-shipping-method-form',
    novaPoshtaFormId:  'novaposhta_form',
    novaPoshtaId:      's_method_borey_novaposhta_standand',
    citySelectId:      'novaposhta_city',
    warehouseSelectId: 'novaposhta_warehouse',
    updateUrl:         '',

    initialize: function (updateUrl) {
        this.updateUrl = updateUrl;
        this.container = $(this.shippingFormId);
        this.container.observe('click', this.parseContent.bind(this));
    },

    parseContent: function(event){
        if (Event.element(event).up('li')) {
            this.novaPoshtaForm = $(this.novaPoshtaFormId);
            if (this.novaPoshtaForm) {
                if (Event.element(event).readAttribute('id') == this.citySelectId) {
                    Event.element(event).onchange = function () {
                        this.changeCountry(Event.element(event));
                    }.bind(this);
                    return;
                }

                if (Event.element(event).readAttribute('id') == this.warehouseSelectId) {
                    Event.element(event).onchange = function () {
                        this.changeWarehouse(Event.element(event));
                    }.bind(this);
                    return;
                }

                var clickedElement = Event.element(event);
                if (clickedElement.readAttribute('name') == 'shipping_method') {
                    if (clickedElement.readAttribute('id') == this.novaPoshtaId) {
                        this.novaPoshtaForm.show()
                    } else {
                        this.novaPoshtaForm.hide();
                    }
                }
            }
        }
    },

    changeCountry: function (element){
        var requestParams = {
            city: element.value
        };
        this.sendUpdate(this.updateUrl,requestParams);
    },

    changeWarehouse: function (element){
        var requestParams = {
            city:       $(this.citySelectId).value,
            warehouse:  element.value
        };
        this.sendUpdate(this.updateUrl,requestParams);
    },


    sendUpdate: function (updateDataUrl, requestParams) {
        new Ajax.Request(updateDataUrl, {
            parameters : requestParams,
            onSuccess  : function(response) {
                if (response.responseText.isJSON()) {
                    var data = response.responseText.evalJSON();

                    if (data.warehouse){
                        var warehouse = $(this.warehouseSelectId);
                        if (warehouse) {
                            warehouse.update(data.warehouse);
                        }
                    }
                    if (data.save){
                        srCheckout.shippingMethodSave();
                    }
                }
            }.bind(this)
        });
    }

});