ProductList = Class.create({}, {
    addToCartButtonClass: null,
    g: new Growler(),

    initialize: function () {
        this.g = new Growler();
        var buttons = $$('.btn-cart');
        buttons.each(function(button){
            var onclick = button.readAttribute('onclick').replace('setLocation', 'productList.sendInfo');
            button.setAttribute('onclick', onclick);
        });
    },

    sendInfo: function(url)
    {
        var _this = this;
        new Ajax.Request(url, {
            parameters : {
                isAjax: 1,
                ajaxOptions: 1
            },
            onCreate   : function() {
                _this.g.info("Processing", {
                    life: 5
                });
            },
            onSuccess  : function(response) {
                if (response.responseText.isJSON()) {
                    var data = response.responseText.evalJSON();
                }
                if (data.status && data.status == 'SUCCESS') {
                    _this.showSuccess(data.message);
                } else {
                    _this.showPopup(data.form);
                }
                var sidebarCart = $$('.block-cart').first();
                var toplinkCart = $$('.header .links').first();
                if(sidebarCart && data.sidebar){
                    sidebarCart.replace(data.sidebar);
                }
                if(toplinkCart && data.toplink){
                    toplinkCart.replace(data.toplink);
                }
            }.bind(this)
        });
    },

    showPopup: function(block) {
        try {
            var _this = this;
            var element = new Element('div', {
                id: 'modalboxOptions',
                class: 'product-view'
            });
            element.update(block);

            var viewport = document.viewport.getDimensions();
            Modalbox.show(element,
                {
                    title: 'Please Select Options',
                    width: 510,
                    height: viewport.height,
                    afterLoad: function() {
                        _this.extractScripts(block);
                        //_this.bindEvents();
                    }
                });
        } catch(e) {
            console.log(e)
        }
    },

    extractScripts: function(strings) {
        this.initProductSubmit();
        var scripts = strings.extractScripts();
        scripts.each(function(script){
            try {
                var newScript = script.replace(/var /gi, "");
                eval(newScript);
            }
            catch(e){
                console.log(e);
            }
        });
    },

    ajaxCartSubmit: function(message)
    {
        if (Modalbox !== 'undefined' && Modalbox.initialized) {
            Modalbox.hide();
        }
        this.showSuccess(message);
    },

    showSuccess: function(message) {
        this.g.info(message, {
            life: 5
        });
    },

    initProductSubmit: function() {
        productAddToCartForm = new VarienForm('product_addtocart_form');

        productAddToCartForm.submit = function(button, url) {
            if (this.validator.validate()) {
                var form = this.form;
                var oldUrl = form.action;
                if (url) {
                    form.action = url;
                }
                var e = null;
                if(!url){
                    url = form.readAttribute('action');
                }
                var data = form.serialize(true);
                data['isAjax'] = 1;
                $('ajax_loader').show();
                try {
                    new Ajax.Request(url, {
                        parameters : data,
                        onSuccess : function(response) {
                            $('ajax_loader').hide();
                            var data = response.responseText.evalJSON();
                            var sidebarCart = $$('.block-cart').first();
                            var toplinkCart = $$('.header .links').first();
                            if(sidebarCart && data.sidebar){
                                sidebarCart.replace(data.sidebar);
                            }
                            if(toplinkCart && data.toplink){
                                toplinkCart.replace(data.toplink);
                            }
                            productList.ajaxCartSubmit(data.message);
                        }
                    });

                } catch (e) {
                }

                this.form.action = oldUrl;
                if (e) {
                    throw e;
                }

                if (button && button != 'undefined') {
                    button.disabled = true;
                }
            }
        }
    }
});

function validateDownloadableCallback(elmId, result) {
    var container = $('downloadable-links-list');
    if (result == 'failed') {
        container.removeClassName('validation-passed');
        container.addClassName('validation-failed');
    } else {
        container.removeClassName('validation-failed');
        container.addClassName('validation-passed');
    }
}

document.observe("dom:loaded", function() {
    productList = new ProductList();
});
