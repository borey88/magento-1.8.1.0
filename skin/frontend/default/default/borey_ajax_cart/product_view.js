ProductVew = Class.create({}, {
    addToCartButtonClass: null,
    g: new Growler(),

    initialize: function() {
        this.initProductSubmit();
    },

    initProductSubmit: function() {
        productAddToCartForm.submit = function(button, url) {
            if (this.validator.validate()) {
                var form = this.form;
                var oldUrl = form.action;
                if (url) {
                    form.action = url;
                }
                var e = null;
                if(!url){
                    url = form.readAttribute('action');
                }
                var data = form.serialize(true);
                data['isAjax'] = 1;
                $('ajax_loader').show();
                try {
                    new Ajax.Request(url, {
                        parameters : data,
                        onSuccess : function(response) {
                            $('ajax_loader').hide();
                            var data = response.responseText.evalJSON();
                            var sidebarCart = $$('.block-cart').first();
                            var toplinkCart = $$('.header .links').first();
                            if(sidebarCart && data.sidebar){
                                sidebarCart.replace(data.sidebar);
                            }
                            if(toplinkCart && data.toplink){
                                toplinkCart.replace(data.toplink);
                            }
                            var g = new Growler();
                            if (data.status == 'SUCCESS') {
                                g.info(data.message, {
                                    life: 5
                                });
                            } else {
                                g.error(data.message, {
                                    life: 5
                                });
                            }
                            button.disabled = false;
                        }
                    });

                } catch (e) {
                }

                this.form.action = oldUrl;
                if (e) {
                    throw e;
                }

                if (button && button != 'undefined') {
                    button.disabled = true;
                }
            }
        }
    }

});

document.observe("dom:loaded", function() {
    var productVew = new ProductVew();
});
