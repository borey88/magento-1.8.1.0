CartWidget = Class.create({}, {
    initialize: function() {
        this.initRemoveButton();
    },

    initRemoveButton: function() {
        var buttons = $$('.btn-remove');
        buttons.each(function(button){
            var onclick = button.readAttribute('onclick').replace('confirm(', 'cartWidget.removeProduct(this, ');
            button.setAttribute('onclick', onclick);
        });
    },

    removeProduct: function(button, message) {
        if (confirm(message)) {
            var url = button.readAttribute('href');
            var data = {
                'isAjax': 1
            };
            var _this = this;
            new Ajax.Request(url, {
                parameters : data,
                onSuccess : function(response) {
                    var data = response.responseText.evalJSON();
                    var sidebarCart = $$('.block-cart').first();
                    var toplinkCart = $$('.header .links').first();
                    if(sidebarCart && data.sidebar){
                        sidebarCart.replace(data.sidebar);
                        _this.initRemoveButton();
                    }
                    if(toplinkCart && data.toplink){
                        toplinkCart.replace(data.toplink);
                    }
                    var g = new Growler();
                    if (data.status == 'SUCCESS') {
                        g.info(data.message, {
                            life: 5
                        });
                    } else {
                        g.error(data.message, {
                            life: 5
                        });
                    }
                }
            });
        }
        return false;
    }
});

document.observe("dom:loaded", function() {
    cartWidget = new CartWidget();
});
