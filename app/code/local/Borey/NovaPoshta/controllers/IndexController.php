<?php

class Borey_NovaPoshta_IndexController extends Mage_Core_Controller_Front_Action
{

    /**
     * Set NovaPoshta params
     */
    public function updateAction()
    {
        $params = $this->getRequest()->getParams();
        $result = array('status' => 'success');
        if (isset($params['warehouse']) && $warehouseId = (int) $params['warehouse']) {
            $this->getOnepage()->getQuote()->getShippingAddress()->setNovaposhtaWarehouseId($warehouseId);
            $this->getOnepage()->getQuote()->collectTotals()->save();
        } else if (isset($params['city']) && $cityId = (int) $params['city']) {
            $result['warehouse'] = $this->_getWarehouseOptions($cityId);
            $this->getOnepage()->getQuote()->getShippingAddress()->setNovaposhtaCityId($cityId);
            $this->getOnepage()->getQuote()->collectTotals()->save();
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Return warehouses options
     *
     * @param Integer $cityId
     * @return String
     */
    protected function _getWarehouseOptions($cityId)
    {
        $cityList = Mage::getResourceModel('borey_novaposhta/warehouse_collection')->getWarehousePairs($cityId);
        return $this->_prepareWarehouseOptions($cityList);
    }

    /**
     * Return prepared options as html-content
     *
     * @param Array $list
     * @return String
     */
    protected function _prepareWarehouseOptions($list)
    {
        $helper = Mage::helper('novaposhta');
        $options = '<option value="">' . $helper->__('-- select warehouse--') . '</option>';
        foreach ($list as $id => $address) {
            $options .= '<option value="' . $id . '">' . $address . '</option>';
        }
        return $options;
    }

    /**
     * Get one page checkout model
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    public function getOnepage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }
}