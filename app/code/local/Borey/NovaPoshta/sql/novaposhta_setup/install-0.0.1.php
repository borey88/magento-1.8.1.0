<?php

$installer = $this;
$tableQuoteAddress = $installer->getTable('sales/quote_address');
$tableOrderAddress = $installer->getTable('sales/order_address');

$installer->startSetup();
$installer->getConnection()->addColumn($tableQuoteAddress, 'novaposhta_city_id', array(
    'comment'   => 'Novaposhta City Id',
    'default'   => null,
    'nullable'  => true,
    'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
));
$installer->getConnection()->addColumn($tableQuoteAddress, 'novaposhta_warehouse_id', array(
    'comment'   => 'Warehouse Id',
    'default'   => null,
    'nullable'  => true,
    'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
));

$installer->getConnection()->addColumn($tableOrderAddress, 'novaposhta_city_id', array(
    'comment'   => 'Novaposhta City Id',
    'default'   => null,
    'nullable'  => true,
    'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
));
$installer->getConnection()->addColumn($tableOrderAddress, 'novaposhta_warehouse_id', array(
    'comment'   => 'Warehouse Id',
    'default'   => null,
    'nullable'  => true,
    'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
));

$installer->endSetup();
