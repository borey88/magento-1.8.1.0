<?php

$installer = $this;
$tableCity = $installer->getTable('borey_novaposhta/city');
$tableWarehouse = $installer->getTable('borey_novaposhta/warehouse');
$installer->startSetup();

$installer->getConnection()->dropTable($tableCity);
$table = $installer->getConnection()
    ->newTable($tableCity)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        'primary'   => true,
    ))
    ->addColumn('name_ru', Varien_Db_Ddl_Table::TYPE_TEXT, '100')
    ->addColumn('name_ua', Varien_Db_Ddl_Table::TYPE_TEXT, '100')
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable'  => false,
    ));
$installer->getConnection()->createTable($table);

$installer->getConnection()->dropTable($tableWarehouse);
$table = $installer->getConnection()
    ->newTable($tableWarehouse)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        'primary'   => true,
    ))
    ->addColumn('city_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false
    ))
    ->addColumn('address_ru', Varien_Db_Ddl_Table::TYPE_TEXT, '200')
    ->addColumn('address_ua', Varien_Db_Ddl_Table::TYPE_TEXT, '200')
    ->addColumn('phone', Varien_Db_Ddl_Table::TYPE_TEXT, '100')
    ->addColumn('weekday_work_hours', Varien_Db_Ddl_Table::TYPE_TEXT, '20')
    ->addColumn('weekday_reseiving_hours', Varien_Db_Ddl_Table::TYPE_TEXT, '20')
    ->addColumn('weekday_delivery_hours', Varien_Db_Ddl_Table::TYPE_TEXT, '20')
    ->addColumn('saturday_work_hours', Varien_Db_Ddl_Table::TYPE_TEXT, '20')
    ->addColumn('saturday_reseiving_hours', Varien_Db_Ddl_Table::TYPE_TEXT, '20')
    ->addColumn('saturday_delivery_hours', Varien_Db_Ddl_Table::TYPE_TEXT, '20')
    ->addColumn('max_weight_allowed', Varien_Db_Ddl_Table::TYPE_INTEGER, '4')
    ->addColumn('longitude', Varien_Db_Ddl_Table::TYPE_FLOAT, array(10, 6))
    ->addColumn('latitude', Varien_Db_Ddl_Table::TYPE_FLOAT, array(10, 6))
    ->addColumn('number_in_city', Varien_Db_Ddl_Table::TYPE_INTEGER, '3', array(
        'nullable'   => false,
        'unsigned '  => true,
    ))
    ->addColumn('updated_a', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable'  => false,
    ))
    ->addForeignKey(
        'city_id',
        'city_id',
        $installer->getTable('borey_novaposhta/city'),
        'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
;
$installer->getConnection()->createTable($table);

$installer->endSetup();