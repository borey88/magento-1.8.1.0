<?php

class Borey_NovaPoshta_Block_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $helper = Mage::helper('novaposhta');
        $form = new Varien_Data_Form();
        $form->addField('novaposhta_city', 'select', array(
            'label'  => $helper->__('City'),
            'values' => $this->_getPreparedCities(),
            'class'  => 'required-entry'
        ));
        $form->addField('novaposhta_warehouse', 'select', array(
            'label'  => $helper->__('Warehouse'),
            'values' => ['' => $helper->__('-- select warehouse--')],
            'class'  => 'required-entry'
        ));

        $formData = [];
        $form->setValues($formData);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Return array of cities with non value first element
     *
     * @return Array
     */
    protected function _getPreparedCities()
    {
        $helper = Mage::helper('novaposhta');
        return array('' => $helper->__('-- select city--')) + $this->_getCities();
    }

    /**
     * Return key-value pairs of cities
     *
     * @return Array
     */
    protected function _getCities()
    {
        return Mage::getResourceModel('borey_novaposhta/city_collection')->getCitiesPairs();
    }

}


