<?php
class Borey_NovaPoshta_Model_City extends Mage_Core_Model_Abstract
{
    /**
     * Model initialization
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('borey_novaposhta/city');
    }

}