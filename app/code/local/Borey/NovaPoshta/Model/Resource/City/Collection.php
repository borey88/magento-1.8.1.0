<?php
class Borey_NovaPoshta_Model_Resource_City_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Collection initialization
     */
    public function _construct()
    {
        $this->_init('borey_novaposhta/city');
        parent::_construct();
    }

    /**
     * Return cities pairs as key-value
     *
     * @return Array
     */
    public function getCitiesPairs()
    {
        return $this->addOrder('name_ua')->addFieldToSelect(array('name_ua'))->getSelect()->query()
            ->fetchAll(PDO::FETCH_KEY_PAIR);
    }
}