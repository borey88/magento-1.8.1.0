<?php
class Borey_NovaPoshta_Model_Resource_Warehouse_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Collection initialization
     */
    public function _construct()
    {
        $this->_init('borey_novaposhta/warehouse');
        parent::_construct();
    }

    /**
     * Return warehouses pairs as key-value
     *
     * @param Integer $cityId
     * @return Array
     */
    public function getWarehousePairs($cityId = null)
    {
        if ($cityId) {
            $this->addFieldToFilter('city_id', $cityId);
        }
        $this->addOrder('address_ua');
        return $this->addFieldToSelect(array('address_ua'))->getSelect()->query()->fetchAll(PDO::FETCH_KEY_PAIR);
    }
}