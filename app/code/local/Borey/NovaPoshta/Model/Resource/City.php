<?php
class Borey_NovaPoshta_Model_Resource_City extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Resource initialization
     */
    public function _construct()
    {
        $this->_init('borey_novaposhta/city', 'id');
    }

}