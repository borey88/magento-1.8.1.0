<?php

class Borey_NovaPoshta_Model_Order extends Mage_Sales_Model_Order
{
    /**
     * Declare order shipping address
     *
     * @param   Mage_Sales_Model_Order_Address $address
     * @return  Mage_Sales_Model_Order
     */
    public function setShippingAddress(Mage_Sales_Model_Order_Address $address)
    {
        $address->setNovaposhtaCityId($this->getQuote()->getShippingAddress()->getNovaposhtaCityId());
        $address->setNovaposhtaWarehouseId($this->getQuote()->getShippingAddress()->getNovaposhtaWarehouseId());
        return parent::setShippingAddress($address);
    }

    /**
     * Return city value
     *
     * @return String
     */
    public function getNovaposhtaCity()
    {
        $cityModel = Mage::getModel('borey_novaposhta/city');
        $cityModel->load($this->getShippingAddress()->getNovaposhtaCityId());
        return $cityModel->getData('name_ua');
    }

    /**
     * Return warehouse value
     *
     * @return String
     */
    public function getNovaposhtaWarehouse()
    {
        $warehouseModel = Mage::getModel('borey_novaposhta/warehouse');
        $warehouseModel->load($this->getShippingAddress()->getNovaposhtaWarehouseId());
        return $warehouseModel->getData('address_ua');
    }

}
