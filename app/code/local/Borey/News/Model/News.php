<?php

class Borey_News_Model_News extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('boreynews/news');
    }

    protected function _afterDelete()
    {
        $helper = Mage::helper('boreynews');
        @unlink($helper->getImagePath($this->getId()));
        return parent::_afterDelete();
    }

    protected function _beforeSave()
    {
        $helper = Mage::helper('boreynews');

        if (!$this->getData('link')) {
            $this->setData('link', $helper->prepareUrl($this->getTitle()));
        } else {
            $this->setData('link', $helper->prepareUrl($this->getData('link')));
        }
        return parent::_beforeSave();
    }

    public function getImageUrl()
    {
        $helper = Mage::helper('boreynews');
        if ($this->getId() && file_exists($helper->getImagePath($this->getId()))) {
            return $helper->getImageUrl($this->getId());
        }
        return null;
    }

}