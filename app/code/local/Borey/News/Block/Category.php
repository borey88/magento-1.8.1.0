<?php

class Borey_News_Block_Category extends Mage_Core_Block_Template
{

    public function getNewsCollection()
    {
        $newsCollection = Mage::getModel('boreynews/category')->getCollection();
        $newsCollection->setOrder('created', 'DESC');
        return $newsCollection;
    }

}