<?php

class Borey_News_Block_Adminhtml_Category extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        $helper = Mage::helper('boreynews');
        $this->_blockGroup = 'boreynews';
        $this->_controller = 'adminhtml_category';

        $this->_headerText = $helper->__('News Categories Management');
        $this->_addButtonLabel = $helper->__('Add Category');
        parent::_construct();
    }

}