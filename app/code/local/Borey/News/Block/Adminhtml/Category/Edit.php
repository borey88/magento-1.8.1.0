<?php

class Borey_News_Block_Adminhtml_Category_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'boreynews';
        $this->_controller = 'adminhtml_category';
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('boreynews');
        $model = Mage::registry('current_category_news');

        if ($model->getId()) {
            return $helper->__("Edit News Category item '%s'", $this->escapeHtml($model->getTitle()));
        } else {
            return $helper->__("Add News Category item");
        }
    }

}