<?php
class Borey_AjaxCart_Model_Observer
{
    public function getProductOptions()
    {
        $request = Mage::app()->getFrontController()->getRequest();
        $params = $request->getParams();
        if(isset($params['isAjax']) && $params['isAjax'] == 1){
            $this->_getAdditionalForm($params);
        }
    }

    protected function _getAdditionalForm($params)
    {
        $product = Mage::registry('current_product');

        $layout = Mage::getSingleton('core/layout');
        $res = '';
        $layout->getUpdate()->addHandle('ajaxcart_configurable_options');

        $layout->getUpdate()->addHandle('PRODUCT_TYPE_' . $product->getTypeId());
        $layout->getUpdate()->addHandle('PRODUCT_' . $product->getId());

        if ($product->getTypeId() == 'bundle')
            $layout->getUpdate()->addHandle('ajaxcart_bundle_options');

        // set unique cache ID to bypass caching
        $cacheId = 'LAYOUT_'.Mage::app()->getStore()->getId().md5(join('__', $layout->getUpdate()->getHandles()));
        $layout->getUpdate()->setCacheId($cacheId);

        $layout->getUpdate()->load();
        $layout->generateXml();
        $layout->generateBlocks();

        $value = $layout->getBlock('ajaxcart.configurable.options');

        if ($value) {
            $res .= $value->toHtml();
        }

        if ($product->getTypeId() == 'bundle') {
            $value = $layout->getBlock('product.info.bundle');

            if ($value) {
                $res .= $value->toHtml();
            }
        }

        Mage::app()->getFrontController()->getResponse()->setHeader('Content-Type', 'text/plain')->setBody(
            Zend_Json::encode(array('form' => $res)));
        Mage::app()->getFrontController()->getResponse()->sendResponse();
        die;
    }

    public function checkAjaxAction(Varien_Event_Observer $observer)
    {
        $request = Mage::app()->getFrontController()->getRequest();
        $params = $request->getParams();
        if(isset($params['isAjax']) && $params['isAjax'] == 1){
            $this->_addAjaxAction($params, $observer);
        }
    }

    protected function _addAjaxAction($params, $observer)
    {
        $helper = Mage::helper('borey_ajaxcart');
        $product = $this->_initProduct();
        if ($product->getTypeId() == Mage_Catalog_Model_Product_Type_Grouped::TYPE_CODE && isset($params['ajaxOptions'])
            && $params['ajaxOptions'] == 1)
        {
            $this->_getGroupedOptions($product);
        } else {
            $response = array();
            if (!$this->_validateFormKey()) {
                $response['status'] = 'ERROR';
                $response['message'] = $helper->__('Cannot add the item to shopping cart.');
            } else {
                $this->_addProductToCart($product, $params, $observer);
            }
        }
    }

    protected function _addProductToCart($product, $params, $observer)
    {
        /** @var Mage_Checkout_CartController $controller */
        $controller = $observer->getData('controller_action');
        $response = array();
        $helper = Mage::helper('borey_ajaxcart');
        $cart   = $this->_getCart();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $related = Mage::app()->getFrontController()->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $response['status'] = 'ERROR';
                $response['message'] = $helper->__('Unable to find Product ID');
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array(
                    'product' => $product,
                    'request' => Mage::app()->getFrontController()->getRequest(),
                    'response' => Mage::app()->getFrontController()->getResponse()
                )
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()){
                    $message = $helper->__('%s was added to your shopping cart.',
                        Mage::helper('core')->htmlEscape($product->getName()));
                    $response['status'] = 'SUCCESS';
                    $response['message'] = $message;

                    $controller->loadLayout();
                    $response['toplink'] = $controller->getLayout()->getBlock('top.links')->toHtml();
                    $response['sidebar'] = $controller->getLayout()->getBlock('cart_sidebar')->toHtml();
                }
            }
        } catch (Mage_Core_Exception $e) {
            $msg = "";
            if ($this->_getSession()->getUseNotice(true)) {
                $msg = $e->getMessage();
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $msg .= $message.'<br/>';
                }
            }

            $response['status'] = 'ERROR';
            $response['message'] = $msg;
        } catch (Exception $e) {
            $response['status'] = 'ERROR';
            $response['message'] = $helper->__('Cannot add the item to shopping cart.');
            Mage::logException($e);
        }

        Mage::app()->getFrontController()->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        Mage::app()->getFrontController()->getResponse()->sendResponse();
        die;
    }

    protected function _getGroupedOptions($product)
    {
        Mage::register('product', $product);
        Mage::register('current_product', $product);

        $layout = Mage::getSingleton('core/layout');
        $res = '';
        $layout->getUpdate()->addHandle('ajaxcart_grouped_options');
        $cacheId = 'LAYOUT_'.Mage::app()->getStore()->getId().md5(join('__', $layout->getUpdate()->getHandles()));
        $layout->getUpdate()->setCacheId($cacheId);

        $layout->getUpdate()->load();
        $layout->generateXml();
        $layout->generateBlocks();

        $value = $layout->getBlock('ajaxcart.grouped.options');
        if ($value) {
            $res .= $value->toHtml();
        }

        Mage::app()->getFrontController()->getResponse()->setHeader('Content-Type', 'text/plain')
            ->setBody(Zend_Json::encode(array('form' => $res)));
        Mage::app()->getFrontController()->getResponse()->sendResponse();
        die;
    }

    /**
     * Validate Form Key
     *
     * @return bool
     */
    protected function _validateFormKey()
    {
        if (!($formKey = Mage::app()->getFrontController()->getRequest()->getParam('form_key', null))
            || $formKey != Mage::getSingleton('core/session')->getFormKey()) {
            return false;
        }
        return true;
    }

    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    public function addActionLayoutHandles(Mage_Core_Model_Layout_Update $update)
    {
        // load store handle
        $update->addHandle('STORE_'.Mage::app()->getStore()->getCode());

        // load theme handle
        $package = Mage::getSingleton('core/design_package');
        $update->addHandle(
            'THEME_'.$package->getArea().'_'.$package->getPackageName().'_'.$package->getTheme('layout')
        );

        // load action handle
        $update->addHandle(strtolower($this->getFullActionName()));

        return $this;
    }

    public function getFullActionName($delimiter='_')
    {
        $request = Mage::app()->getFrontController()->getRequest();
        return $request->getRequestedRouteName().$delimiter.
        $request->getRequestedControllerName().$delimiter.
        $request->getRequestedActionName();
    }

    protected function _initProduct()
    {
        $productId = (int) Mage::app()->getFrontController()->getRequest()->getParam('product');
        if ($productId) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);
            if ($product->getId()) {
                return $product;
            }
        }
        return false;
    }

    public function updateItemOptions(Varien_Event_Observer $observer)
    {
        $params = Mage::app()->getFrontController()->getRequest()->getParams();
        if (isset($params['isAjax']) && $params['isAjax'] == 1) {
            $response = array();
            $helper = Mage::helper('borey_ajaxcart');
            $cart   = $this->_getCart();
            $id = (int) Mage::app()->getFrontController()->getRequest()->getParam('id');

            if (!isset($params['options'])) {
                $params['options'] = array();
            }
            try {
                if (isset($params['qty'])) {
                    $filter = new Zend_Filter_LocalizedToNormalized(
                        array('locale' => Mage::app()->getLocale()->getLocaleCode())
                    );
                    $params['qty'] = $filter->filter($params['qty']);
                }

                $quoteItem = $cart->getQuote()->getItemById($id);
                if (!$quoteItem) {
                    Mage::throwException($helper->__('Quote item is not found.'));
                }

                $item = $cart->updateItem($id, new Varien_Object($params));
                if (is_string($item)) {
                    Mage::throwException($item);
                }
                if ($item->getHasError()) {
                    Mage::throwException($item->getMessage());
                }

                $related = $params['related_product'];
                if (!empty($related)) {
                    $cart->addProductsByIds(explode(',', $related));
                }

                $cart->save();

                $this->_getSession()->setCartWasUpdated(true);

                Mage::dispatchEvent('checkout_cart_update_item_complete',
                    array(
                        'item'     => $item,
                        'request'  => Mage::app()->getFrontController()->getRequest(),
                        'response' => Mage::app()->getFrontController()->getResponse()
                    )
                );
                $response['status'] = 'SUCCESS';
                if (!$this->_getSession()->getNoCartRedirect(true)) {
                    if (!$cart->getQuote()->getHasError()) {
                        $message = $helper->__('%s was updated in your shopping cart.',
                            Mage::helper('core')->escapeHtml($item->getProduct()->getName()));
                        $response['message'] = $message;
                        $controller = $observer->getData('controller_action');
                        $controller->loadLayout();
                        $response['toplink'] = $controller->getLayout()->getBlock('top.links')->toHtml();
                        $response['sidebar'] = $controller->getLayout()->getBlock('cart_sidebar')->toHtml();
                    }
                }

            } catch (Mage_Core_Exception $e) {
                $response['status'] = 'ERROR';
                if ($this->_getSession()->getUseNotice(true)) {
                    $response['message'] = $e->getMessage();
                } else {
                    $response['message'] = $e->getMessage();
                }
            } catch (Exception $e) {
                $response = array(
                    'status'  => 'ERROR',
                    'message' => $helper->__('Cannot update the item.')
                );
                Mage::logException($e);
            }

            Mage::app()->getFrontController()->getResponse()->setHeader('Content-Type', 'text/plain')->setBody(
                Zend_Json::encode($response));
            Mage::app()->getFrontController()->getResponse()->sendResponse();
            die;
        }
    }

    public function deleteItem(Varien_Event_Observer $observer)
    {
        $params = Mage::app()->getFrontController()->getRequest()->getParams();
        if (isset($params['isAjax']) && $params['isAjax'] == 1) {
            $response = array();
            $helper = Mage::helper('borey_ajaxcart');

            $id = (int) Mage::app()->getFrontController()->getRequest()->getParam('id');
            if ($id) {
                try {
                    $this->_getCart()->removeItem($id)
                        ->save();
                    $response['status'] = 'SUCCESS';
                    $response['message'] = $helper->__('Item has been removed');
                    $controller = $observer->getData('controller_action');
                    $controller->loadLayout();
                    $response['toplink'] = $controller->getLayout()->getBlock('top.links')->toHtml();
                    $response['sidebar'] = $controller->getLayout()->getBlock('cart_sidebar')->toHtml();
                } catch (Exception $e) {
                    $response['status'] = 'ERROR';
                    $response['message'] = $helper->__('Cannot remove the item.');
                    Mage::logException($e);
                }
                Mage::app()->getFrontController()->getResponse()->setHeader('Content-Type', 'text/plain')->setBody(
                    Zend_Json::encode($response));
                Mage::app()->getFrontController()->getResponse()->sendResponse();
                die;
            }

        }
    }
}